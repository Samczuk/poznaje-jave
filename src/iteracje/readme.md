# Instrukcje iteracyjne - p�tle

dop�ki spe�niony jest zadany warunek to powtarzaj� blok kodu

## while

dop�ki spe�niony jest zadawy warunek to wykonywany jest blok kodu

```java
int i = 1;
while (i <= 3) {
	System.out.println("i = " + i);
	i += 1;
}
```
da wynik:

```
i = 1
i = 2
i = 3
```

* najpierw sprawdza warunek w nawiasie (czy i <= 3)
* je�eli warunek == true to wykonuje blok kodu w klamerkach {}
    * po wykonaniu bloku sprawdza warunek jeszcze raz, i tak w k�ko
    * wykonuje blok "dop�ki" (while) warunek jest prawdziwy (dop�ki i <= 3)
* je�eli warunek == false - program przechodzi poza p�tle while

### Uwaga:
```java
int i = 1;
while (i <= 3) {
	System.out.println("i = " + i);
	// i += 1; - program nigdy nie wyjdzie z p�tli while, poniewa� warunek zawsze b�dzie spe�niony
}
```



## do while

wykonuje blok kodu i powtarza go dop�ki zadany warunek jest spe�niony


```java
char znak = '1';
do {
	System.out.println("Prosz� wpisa� 1");
	znak = (char) System.in.read(); // pobiera znak (znaki) z konsoli - czeka na enter
	System.out.println("\"" + znak + "\""); // pokazuje co zosta�o wpisane
} while (znak != '1');

System.out.println("Dzi�kuj�");
```

w do while blok kodu wykona si� przynajmniej raz, nawet je�eli warunek nie jest spe�niony nawet za pierwszym razem 


## for

dop�ki spe�niony jest zadany warunek to powtarzaj� blok kodu

dla (kod do wykonania przed p�tl� ; warunek ; kod do wykonania po ka�dorazowym wykonaniu cia�a p�tli) { cia�o p�tli }
* kod do wykonania przed p�tl�
     * wykonuje tylko raz, przed pierwszym sprawdzeniem warunku
     * mo�na tam zdefiniowa� zmienn�, kt�ra b�dzie dost�pna w p�tli, zmienna ta nie istnieje poza p�tl�
* kod do wykonania po ka�dorazowym wykonaniu cia�a p�tli
     * je�eli warunek nie zostanie spe�niony - nie wykona si� cia�o p�tli i nie wykona si� te� ten kod

```java
for (int i = 1; i <= 3; i++) {
	System.out.println("i = " + i);
}
```
da wynik
```
i = 1
i = 2
i = 3
```

## for (for each)

p�tle for mo�na tak�e wykorzysta� w uproszczony spos�b do iterowania po kolekcjach

np.

mamy tablic� String'�w

```java
String[] imiona = new String[]{ "Kuba", "Kamila", "Anastazja", "Cheo, psubrat" };
```
i chcemy wypisa� na ekran wszystkie elementy tej tablicy

Mo�emy do tego u�y� standardowego for'a:

```java
for (int i = 0; i < imiona.length; ++i) {
	System.out.println(imiona[i]);
}
```

Ale mo�na zrobi� to pro�ciej przy u�yciu p�tli for:

```java
for (String imie : imiona) {
	System.out.println(imie);
}
```

