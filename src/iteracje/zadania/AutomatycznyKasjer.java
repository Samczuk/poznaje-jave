package iteracje.zadania;

public class AutomatycznyKasjer {
	
	public static void main(String[] args) {
		// klient w kasie samoobs�ugowej za zakupy ma zap�aci� 39 z�otych
		// klient do zap�aty u�ywa banknotu 500 z�otych
		// kasjer ma wyda� reszt� w jak najmniejszej ilo�ci banknot�w / monet
		// kasjer ma do wyboru banknoty 200, 100, 50, 20, 10 z�otych, i monety 5, 2, 1 z�
		
		
		int kwotaDoZap�aty = 39;
		int zaplacono = 500;
		
		// masz 2 tablice, pierwsza to tablica nomina��w. Druga to tablica, w kt�rej ma si� znalez� ilo�� banknot�w do wydania.
		// np. nominaly[0] == 200, ileBanknotowODanymNominale[0] powinno by� r�wne 2, poniewa� automat powinien wyda� 2 banknoty 200 z�otowe, 
		
		int[] nominaly = new int[] {200, 100, 50, 20, 10, 5, 2, 1};
		int[] ileBanknotowODanymNominale = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
		
		// tutaj napisz kod, kt�ry to policzy
		
		
		// a tutaj kod, kt�ry wypisze na ekran tylko te nomina�y, kt�rych ilo�� jest wi�ksza ni� 0:
		// reszta:
		// nomina� 200 z�, ilo��: 2
		// nomina� 50 z�, ilo��: 1
		// nomina� 10 z�, ilo��: 1
		// nomina� 1 z�, ilo��: 1
	}
}
