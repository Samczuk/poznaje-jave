package iteracje.zadania;

import java.util.Random;

public class WolnyRynek {
	public static void main(String[] args) {
		
		// Andrzej i Bartek zacz�li gra� na gie�dzie kryptowalut
		// za�o�yli si� mi�dzy sob� kto ostatni zbankrutuje.
		// Ka�dego dnia z ich konta ubywa losowa ilo�� pieni�dzy (wykorzystaj w tym celu metod� "ileHajsuZabralWolnyRynek").
		// ile pieni�dzy traci Andrzej = ileHajsuZabralWolnyRynek(saldoKontaAndrzeja)
		// na ko�cu wypisz w ile dni zbankrutowa� pierwszy z nich oraz kto to by� (a mo�e zbankturowali obydwoje w tym samym czasie)
		
		int saldoKontaAndrzeja = 100;
		int saldoKontaBartka = 100;
		
		
		
	}
	
	private static Random random = new Random();
	
	private static int ileHajsuZabralWolnyRynek(int ileAktualnieMaPieniedzy) {
		if (ileAktualnieMaPieniedzy <= 0) {
			return 0; //nie okradamy ubogich i zad�u�onych
		}
		return Math.abs(random.nextInt()) % ileAktualnieMaPieniedzy;
	}
}
