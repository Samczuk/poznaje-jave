package iteracje.przyklady;

import java.io.IOException;

public class DoWhile {
	
	public static void main(String[] args) throws IOException {
		
		char znak;
		do {
			System.out.println("Prosz� wpisa� 1");
			znak = (char) System.in.read(); // pobiera znak (znaki) z konsoli - czeka na enter
			System.out.println("\"" + znak + "\""); // pokazuje co zosta�o wpisane
		} while (znak != '1');
		
		System.out.println("Dzi�kuj�");
		
		// w warunku mo�na wykonywa� metody. Wa�ne, �eby ostateczny wynik by� boolean'em
		do {
			System.out.println("Prosz� wpisa� 2");
		} while ('2' != (char) System.in.read());
		
		System.out.println("Dzi�kuj�");
	}
}
