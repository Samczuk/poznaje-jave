package iteracje.przyklady;

public class For {
	
	public static void main(String[] args) {
		int i = 1;
		for (przedForem(); i <= 3; poKazdymKroku()) {
			System.out.println("i = " + i++);
		}
	}
	
	private static int przedForem() {
		System.out.println("wywo�ano przedForem");
		return 1;
	}
	
	private static void poKazdymKroku() {
		System.out.println("wywo�ano poKazdymKroku");
	}
}
