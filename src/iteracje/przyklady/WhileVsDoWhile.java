package iteracje.przyklady;

public class WhileVsDoWhile {

	public static void main(String[] args) {
		int i = 10;
		
		while (i < 5) {
			System.out.println("WHILE, i = " + i);
		}
		
		do {
			System.out.println("DO WHILE, i = " + i);
		} while (i < 5);
	}
}
